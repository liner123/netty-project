package com.athl.netty.netty.ioboundhandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author hl
 * @Data 2020/9/3
 */
public class MyByteToLongDecoder extends ByteToMessageDecoder {

    /**
     * decode方法会根据接收的数据，被调用多次，直到确定没有新的元素被添加到List,或者ByteBuf没有更多可读字节
     * 如果List Out不为空，就会将List的内容传递给下一个channelInBoundHandler处理，该处理器方法也会被调用多次
     *
     * @param ctx 上下文对象
     * @param in  入站的 ByteBuf
     * @param out List集合 , 解码后传入下一个handler
     * @throws Exception
     */
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        System.out.println("MyByteToLongDecoder 被调用");
        // Long = 8 字节
        int num = 8;
        if (in.readableBytes() >= num) {
            out.add(in.readLong());
        }
    }
}

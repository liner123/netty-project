package com.athl.netty.netty.ioboundhandler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * @author hl
 * @Data 2020/9/3
 */
public class MyClientInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("MyLongToByte", new MyLongToByteEncoder());
        pipeline.addLast("MyByteToLong", new MyByteToLongDecoder());
        pipeline.addLast("MyClientHandler", new MyClientHandler());
    }
}

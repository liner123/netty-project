package com.athl.netty.netty.ioboundhandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * @author hl
 * @Data 2020/9/3
 */
public class MyByteToLongDecoder2 extends ReplayingDecoder<Void> /*内部自己做数据处理*/ {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        out.add(in.readLong());
    }
}

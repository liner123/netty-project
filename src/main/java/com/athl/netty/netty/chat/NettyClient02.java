package com.athl.netty.netty.chat;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.Scanner;

/**
 * @author hl
 * @Data 2020/9/2
 */
public class NettyClient02 {

    private final String host;
    private final int port;

    public NettyClient02(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void run() {
        EventLoopGroup clientGroup = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(clientGroup)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast("decoder", new StringDecoder());
                            pipeline.addLast("encoder", new StringEncoder());
                            pipeline.addLast("myHandler", new NettyClientHandler());
                        }
                    });
            ChannelFuture channelFuture = bootstrap.connect(host, port).sync();
            Channel channel = channelFuture.channel();
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                String msg = scanner.nextLine();
                // 通过channel发送传到服务器端
                channel.writeAndFlush(msg + "\r\n");
            }
        } catch (Exception e) {
            System.out.println("客户端出现了异常 e = " + e);
        } finally {
            clientGroup.shutdownGracefully();
        }

    }

    public static void main(String[] args) {
        new NettyClient02("127.0.0.1", 6666).run();
    }
}

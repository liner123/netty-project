package com.athl.netty.netty.protocoltcp;

/**
 * @author hl
 * @Data 2020/9/4
 */
public class MessageProtocol {

    private int len; // 关键
    private byte[] content;

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}

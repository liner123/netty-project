package com.athl.netty.netty.protocoltcp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * @author hl
 * @Data 2020/9/4
 */
public class MyMessageDecoder extends ReplayingDecoder<Void> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        int length = in.readInt();
        byte[] content = new byte[length];
        in.readBytes(content);
        // 封装成messageProtocol
        MessageProtocol mpl = new MessageProtocol();
        mpl.setLen(length);
        mpl.setContent(content);
        out.add(mpl);
    }
}

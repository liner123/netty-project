package com.athl.netty.netty.codec;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author hl
 * @Data 2020/8/31
 * <p>
 * 自定义handler需要继承某一个 适配器
 */
public class CoderServerHandler extends SimpleChannelInboundHandler<StudentPOJO.Student> {

    /**
     * 读取数据实际
     *
     * @param ctx 上下文对象，含有pipeline ,channel ,地址
     * @param msg client 发送的数据
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, StudentPOJO.Student msg) throws Exception {
        // 读取客户端发送的StudentPOJO.student
        System.out.println("收到消息了");
        System.out.println("id=" + msg.getId() + "名字=" + msg.getName());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // 一般发生异常需要关闭通道
        ctx.close();
    }
}

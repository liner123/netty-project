package com.athl.netty.netty.codec2;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author hl
 * @Data 2020/8/31
 * <p>
 * 自定义handler需要继承某一个 适配器
 */
public class CoderServerHandler extends SimpleChannelInboundHandler<MyDataPOJO.MyMessage> {

    /**
     * 读取数据实际
     *
     * @param ctx 上下文对象，含有pipeline ,channel ,地址
     * @param msg client 发送的数据
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MyDataPOJO.MyMessage msg) throws Exception {
        MyDataPOJO.MyMessage.DataType dataType = msg.getDataType();
        if (dataType == MyDataPOJO.MyMessage.DataType.studentType) {
            MyDataPOJO.Student student = msg.getStudent();
            System.out.println("学生的id=" + student.getId() + "名字=" + student.getName());
        } else if (dataType == MyDataPOJO.MyMessage.DataType.workerType) {
            MyDataPOJO.Worker worker = msg.getWorker();
            System.out.println("学生的age=" + worker.getAge() + "名字=" + worker.getName());
        } else {
            System.out.println("传输类型错误");
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // 一般发生异常需要关闭通道
        ctx.close();
    }


}

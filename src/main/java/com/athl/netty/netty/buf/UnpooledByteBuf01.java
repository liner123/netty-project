package com.athl.netty.netty.buf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

/**
 * @author hl
 * @Data 2020/9/1
 */
public class UnpooledByteBuf01 {
    public static void main(String[] args) {
        // 创建一个 buffer  byte[10]
        // 不需要flip
        // readerIndex , writerIndex , capacity 将 buffer分成三个区域
        // 0 - readerIndex 已经读取的区域
        // readerIndex - writerIndex 可读的区域
        // writerIndex - capacity 可写的区域
        ByteBuf buffer = Unpooled.buffer(10);
        for (int i = 0; i < 10; i++) {
            buffer.writeByte(i);
        }
        // readIndex 不变
        for (int i = 0; i < buffer.capacity(); i++) {
            System.out.println(buffer.getByte(i));
        }
        // readIndex++
        for (int i = 0; i < buffer.capacity(); i++) {
            System.out.println(buffer.readByte());
        }
    }
}

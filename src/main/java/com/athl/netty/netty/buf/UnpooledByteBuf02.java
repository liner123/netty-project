package com.athl.netty.netty.buf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

/**
 * @author hl
 * @Data 2020/9/1
 */
public class UnpooledByteBuf02 {

    public static void main(String[] args) {
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello,world", CharsetUtil.UTF_8);
        if (byteBuf.hasArray()) {
            byte[] context = byteBuf.array();
            String s = new String(context, CharsetUtil.UTF_8);
            System.out.println(s);
            System.out.println("byteBuf =" + byteBuf.getClass());
        }
        byteBuf.arrayOffset();
        byteBuf.readerIndex();
        byteBuf.writerIndex();
        byteBuf.capacity();
        byteBuf.readByte();
        byteBuf.readableBytes();
    }
}

package com.athl.netty.netty.simple;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

import java.util.concurrent.TimeUnit;

/**
 * @author hl
 * @Data 2020/8/31
 * <p>
 * 自定义handler需要继承某一个 适配器
 */
public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    /**
     * 读取数据实际
     *
     * @param ctx 上下文对象，含有pipeline ,channel ,地址
     * @param msg client 发送的数据
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        // 如果有特别耗时的业务 --> 异步执行 --> 提交到当前channel对应的TaskQueue执行
        // 解决方法:
        ctx.channel().eventLoop().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10 * 1000);
                    ctx.writeAndFlush(Unpooled.copiedBuffer("hello,客户端,我是一个大任务", CharsetUtil.UTF_8));
                } catch (InterruptedException e) {
                    System.out.println("出现了异常" + e);
                }
            }
        });
        ctx.channel().eventLoop().schedule(new Runnable() {
            @Override
            public void run() {
                ctx.writeAndFlush(Unpooled.copiedBuffer("hello,客户端，我是一个定时任务", CharsetUtil.UTF_8));
            }
        }, 1, TimeUnit.SECONDS);

//        System.out.println("server ctx=" + ctx);
//        // 将msg转换为byteBuffer
//        ByteBuf buf = (ByteBuf) msg;
//        System.out.println("client msg=" + buf.toString(CharsetUtil.UTF_8));
//        System.out.println("client 地址=" + ctx.channel().remoteAddress());
    }

    /**
     * 数据读取完毕后
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        // write + flush
        // 将数据写入缓存,并刷新
        // 一般来说，我们需要解码
        ctx.writeAndFlush(Unpooled.copiedBuffer("hello,客户端", CharsetUtil.UTF_8));
        System.out.println("go on");
    }

    /**
     * 处理异常
     *
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        // 一般发生异常需要关闭通道
        ctx.close();
    }
}

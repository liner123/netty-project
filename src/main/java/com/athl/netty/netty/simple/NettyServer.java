package com.athl.netty.netty.simple;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author hl
 * @Data 2020/8/31
 */
public class NettyServer {

    public static void main(String[] args) throws InterruptedException {
        // 创建两个线程组，boosGroup and workerGroup
        // bossGroup 处理连接请求，业务交给workerGroup
        // 两个都是无限循环 while(true)
        // bossGroup 和 workerGroup 含有子线程(NioEventLoop)的个数
        // 默认为 cpu核数 * 2
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            // 创建服务器端启动的对象，配置参数
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)// 设置两个线程组
                    .channel(NioServerSocketChannel.class) // 设置NioServerSocketChannel作为服务端实现
                    .option(ChannelOption.SO_BACKLOG, 128) // 设置线程队列连接个数
                    .childOption(ChannelOption.SO_KEEPALIVE, true) // 保存活跃状态
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        // 给pipeline设置处理器
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            // 可以使用一个集合管理SocketChannel ，再次推送消息，可以将业务加入到各个channel对应的
                            // NioEventLoop的 taskQueue 或者 scheduleTaskQueue
                            ch.pipeline().addLast(new NettyServerHandler());
                        }
                    }); // 设置boosGroup and workerGroup的管道处理器
            System.out.println("服务器准备好了。。。");
            ChannelFuture cf = serverBootstrap.bind(6666).sync();// 启动服务器，绑定端口
            cf.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    if (cf.isSuccess()) {
                        System.out.println("监听端口 成功");
                    } else {
                        System.out.println("监听端口 失败");
                    }
                }
            });
            cf.channel().closeFuture().sync(); // 对关闭通道进行监听
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

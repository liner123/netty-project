package com.athl.netty.netty.heart;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author hl
 * @Data 2020/9/3
 */
public class ToIntegerDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        int byteNum = 4;
        if (in.readableBytes() >= byteNum) {
            out.add(in.readInt());
        }
    }
}

package com.athl.netty.netty.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;


/**
 * @author hl
 * @Data 2020/9/1
 */
public class TestServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        // netty 提供的处理http的编解码器
        pipeline.addLast("myHttpServerCodec", new HttpServerCodec());
        // 增加一个自定义的handler
        pipeline.addLast("myHttpServerHandler", new TestHttpHandlerServer());
    }
}

package com.athl.netty.nio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author hl
 * @Data 2020/8/28
 */
public class NIOFileChannel01 {

    public static void main(String[] args) throws Exception {
//        writeChannel();
//        readChannel();
//        copyChannel();
//        copyChannelByTran();
    }

    /**
     * 使用TransferFrom 进行拷贝
     */
    public static void copyChannelByTran() throws Exception {
        FileInputStream fileInputStream = new FileInputStream("src\\main\\1.txt");
        FileChannel channel01 = fileInputStream.getChannel();
        FileOutputStream fileOutputStream = new FileOutputStream("src\\main\\3.txt");
        FileChannel channel02 = fileOutputStream.getChannel();
        channel02.transferFrom(channel02, 0, channel01.size());
        channel01.close();
        channel02.close();
        fileInputStream.close();
        fileOutputStream.close();
    }

    /**
     * 使用一个buffer
     * fileChannel进行拷贝
     */
    public static void copyChannel() throws Exception {
        FileInputStream fileInputStream = new FileInputStream("src\\main\\1.txt");
        FileChannel channel01 = fileInputStream.getChannel();
        File file = new File("src\\main\\2.txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        FileChannel channel02 = fileOutputStream.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(5);
        while (true) {
            /**
             *     public final Buffer clear() {
             *         position = 0;
             *         limit = capacity;
             *         mark = -1;
             *         return this;
             *     }
             */
            // 清空buffer  重要！！！
            byteBuffer.clear();
            int read = channel01.read(byteBuffer);
            if (read == -1) {
                break;
            }
            // 转为写出模式
            byteBuffer.flip();
            channel02.write(byteBuffer);
        }
        fileInputStream.close();
        fileOutputStream.close();
    }

    /**
     * 使用channel从文件读数据并写入控制台
     */
    public static void readChannel() throws Exception {
        File file = new File("F:\\file01.txt");
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel channel = fileInputStream.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate((int) file.length());
        // 将数据从通道读到缓冲区
        channel.read(byteBuffer);
        // 将字节转为字符串
        System.out.println(new String(byteBuffer.array()));
        fileInputStream.close();
    }

    /**
     * 将 hello,channel 写入 file01.txt文件
     *
     * @throws Exception
     */
    public static void writeChannel() throws Exception {
        String str = "hello,channel";
        FileOutputStream fileOutputStream = new FileOutputStream("F:\\file01.txt");
        FileChannel channel = fileOutputStream.getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(str.getBytes());
        byteBuffer.flip();
        // 将byteBuffer的数据写入channel
        channel.write(byteBuffer);
        fileOutputStream.close();
    }
}

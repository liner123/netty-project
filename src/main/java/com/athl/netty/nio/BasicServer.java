package com.athl.netty.nio;

import java.nio.IntBuffer;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author hl
 * @Data 2020/8/28
 */
public class BasicServer {

    public static final ThreadPoolExecutor executorService = new ThreadPoolExecutor(10, 20, 30L, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

    public static void main(String[] args) {

    }

    public static void channelDemo() {
        // FileSocketChannel 用于文件数据读写
        // DatagramChannel 用于UDP数据的读写
        // ServerSocketChannel 用于TCP数据的读写
//        Channel创建流程
        // 1、服务端 创建 ServerSocketChannel(ServerSocketChannelImpl)
        // 2、客户端连接 ServerSocketChannel时创建一个SocketChannel(SocketChannelImpl)
        // 3、通过 SocketChannel进行通信
    }

    /**
     * Selector 方法
     * Selector.select() 阻塞
     * Selector.select(1000) 等待1000ms后返回
     * Selector.selectNow() 直接返回
     * Selector.wakeup() 唤醒阻塞中的
     */

    public static void bufferDemo() {
        // buffer的使用
        // 创建一个容量为 5 的buffer   mark = -1 , position = 0 , limit = 5 , capacity = 5
        IntBuffer intBuffer = IntBuffer.allocate(5);
//        new IntBuffer(-1, 0, 10, 15, new int[], -1);
        // Position : 0 --> 5
        for (int i = 0; i < intBuffer.capacity(); i++) {
            intBuffer.put(i * 2);
        }
        // 读写切换 !!!  --> Position = 0
        intBuffer.flip();
        // Position : 0 --> 5
        while (intBuffer.hasRemaining()) {
            System.out.println(intBuffer.get());
        }
    }

}

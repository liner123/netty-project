package com.athl.netty.nio;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @author hl
 * @Data 2020/8/29
 */
public class NioClient {

    public static void main(String[] args) throws Exception {
        // 拿到socketChannel
        SocketChannel socketChannel = SocketChannel.open();
        // 设置非阻塞
        socketChannel.configureBlocking(false);
        // 提供 host + port
        InetSocketAddress inetSocketAddress = new InetSocketAddress("127.0.0.1", 6666);
        if (!socketChannel.connect(inetSocketAddress)) {
            // 未连接 ，就可以去做其他事
            System.out.println("做其他事情中。。。");
        }
        // 连接成功
        String str = "hello,Nio";
        // Wraps a byte array into a buffer.
        ByteBuffer byteBuffer = ByteBuffer.wrap(str.getBytes());
        // 将buffer 写入 channel
        socketChannel.write(byteBuffer);
        System.in.read();
    }
}
